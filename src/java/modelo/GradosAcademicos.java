/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "grados_academicos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GradosAcademicos.findAll", query = "SELECT g FROM GradosAcademicos g")
    , @NamedQuery(name = "GradosAcademicos.findById", query = "SELECT g FROM GradosAcademicos g WHERE g.id = :id")
    , @NamedQuery(name = "GradosAcademicos.findByNombre", query = "SELECT g FROM GradosAcademicos g WHERE g.nombre = :nombre")})
public class GradosAcademicos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "gradosAcademicos")
    private List<GradosIngeniero> gradosIngenieroList;

    public GradosAcademicos() {
    }

    public GradosAcademicos(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @XmlTransient
    public List<GradosIngeniero> getGradosIngenieroList() {
        return gradosIngenieroList;
    }

    public void setGradosIngenieroList(List<GradosIngeniero> gradosIngenieroList) {
        this.gradosIngenieroList = gradosIngenieroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GradosAcademicos)) {
            return false;
        }
        GradosAcademicos other = (GradosAcademicos) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.GradosAcademicos[ id=" + id + " ]";
    }
    
}
