/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "requisitos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Requisitos.findAll", query = "SELECT r FROM Requisitos r")
    , @NamedQuery(name = "Requisitos.findById", query = "SELECT r FROM Requisitos r WHERE r.requisitosPK.id = :id")
    , @NamedQuery(name = "Requisitos.findByProyectosId", query = "SELECT r FROM Requisitos r WHERE r.requisitosPK.proyectosId = :proyectosId")
    , @NamedQuery(name = "Requisitos.findByDescripcion", query = "SELECT r FROM Requisitos r WHERE r.descripcion = :descripcion")
    , @NamedQuery(name = "Requisitos.findByTipo", query = "SELECT r FROM Requisitos r WHERE r.tipo = :tipo")
    , @NamedQuery(name = "Requisitos.findByImplementado", query = "SELECT r FROM Requisitos r WHERE r.implementado = :implementado")})
public class Requisitos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RequisitosPK requisitosPK;
    @Size(max = 255)
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "tipo")
    private Character tipo;
    @Column(name = "implementado")
    private Short implementado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "requisitos")
    private List<Solicitudes> solicitudesList;
    @JoinColumn(name = "proyectos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proyectos proyectos;

    public Requisitos() {
    }

    public Requisitos(RequisitosPK requisitosPK) {
        this.requisitosPK = requisitosPK;
    }

    public Requisitos(int id, int proyectosId) {
        this.requisitosPK = new RequisitosPK(id, proyectosId);
    }

    public RequisitosPK getRequisitosPK() {
        return requisitosPK;
    }

    public void setRequisitosPK(RequisitosPK requisitosPK) {
        this.requisitosPK = requisitosPK;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public Short getImplementado() {
        return implementado;
    }

    public void setImplementado(Short implementado) {
        this.implementado = implementado;
    }

    @XmlTransient
    public List<Solicitudes> getSolicitudesList() {
        return solicitudesList;
    }

    public void setSolicitudesList(List<Solicitudes> solicitudesList) {
        this.solicitudesList = solicitudesList;
    }

    public Proyectos getProyectos() {
        return proyectos;
    }

    public void setProyectos(Proyectos proyectos) {
        this.proyectos = proyectos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (requisitosPK != null ? requisitosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Requisitos)) {
            return false;
        }
        Requisitos other = (Requisitos) object;
        if ((this.requisitosPK == null && other.requisitosPK != null) || (this.requisitosPK != null && !this.requisitosPK.equals(other.requisitosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Requisitos[ requisitosPK=" + requisitosPK + " ]";
    }
    
}
