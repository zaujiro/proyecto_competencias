/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "seminarios_ingenieros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SeminariosIngenieros.findAll", query = "SELECT s FROM SeminariosIngenieros s")
    , @NamedQuery(name = "SeminariosIngenieros.findById", query = "SELECT s FROM SeminariosIngenieros s WHERE s.seminariosIngenierosPK.id = :id")
    , @NamedQuery(name = "SeminariosIngenieros.findByIngenierosId", query = "SELECT s FROM SeminariosIngenieros s WHERE s.seminariosIngenierosPK.ingenierosId = :ingenierosId")
    , @NamedQuery(name = "SeminariosIngenieros.findBySeminariosId", query = "SELECT s FROM SeminariosIngenieros s WHERE s.seminariosIngenierosPK.seminariosId = :seminariosId")
    , @NamedQuery(name = "SeminariosIngenieros.findByFechaInicio", query = "SELECT s FROM SeminariosIngenieros s WHERE s.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "SeminariosIngenieros.findByFechaFin", query = "SELECT s FROM SeminariosIngenieros s WHERE s.fechaFin = :fechaFin")})
public class SeminariosIngenieros implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SeminariosIngenierosPK seminariosIngenierosPK;
    @Size(max = 45)
    @Column(name = "fecha_inicio")
    private String fechaInicio;
    @Size(max = 45)
    @Column(name = "fecha_fin")
    private String fechaFin;
    @JoinColumn(name = "ingenieros_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingenieros ingenieros;
    @JoinColumn(name = "seminarios_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Seminarios seminarios;

    public SeminariosIngenieros() {
    }

    public SeminariosIngenieros(SeminariosIngenierosPK seminariosIngenierosPK) {
        this.seminariosIngenierosPK = seminariosIngenierosPK;
    }

    public SeminariosIngenieros(int id, int ingenierosId, int seminariosId) {
        this.seminariosIngenierosPK = new SeminariosIngenierosPK(id, ingenierosId, seminariosId);
    }

    public SeminariosIngenierosPK getSeminariosIngenierosPK() {
        return seminariosIngenierosPK;
    }

    public void setSeminariosIngenierosPK(SeminariosIngenierosPK seminariosIngenierosPK) {
        this.seminariosIngenierosPK = seminariosIngenierosPK;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Ingenieros getIngenieros() {
        return ingenieros;
    }

    public void setIngenieros(Ingenieros ingenieros) {
        this.ingenieros = ingenieros;
    }

    public Seminarios getSeminarios() {
        return seminarios;
    }

    public void setSeminarios(Seminarios seminarios) {
        this.seminarios = seminarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (seminariosIngenierosPK != null ? seminariosIngenierosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeminariosIngenieros)) {
            return false;
        }
        SeminariosIngenieros other = (SeminariosIngenieros) object;
        if ((this.seminariosIngenierosPK == null && other.seminariosIngenierosPK != null) || (this.seminariosIngenierosPK != null && !this.seminariosIngenierosPK.equals(other.seminariosIngenierosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.SeminariosIngenieros[ seminariosIngenierosPK=" + seminariosIngenierosPK + " ]";
    }
    
}
