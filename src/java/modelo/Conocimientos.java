/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "conocimientos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Conocimientos.findAll", query = "SELECT c FROM Conocimientos c")
    , @NamedQuery(name = "Conocimientos.findById", query = "SELECT c FROM Conocimientos c WHERE c.conocimientosPK.id = :id")
    , @NamedQuery(name = "Conocimientos.findByIngenierosId", query = "SELECT c FROM Conocimientos c WHERE c.conocimientosPK.ingenierosId = :ingenierosId")
    , @NamedQuery(name = "Conocimientos.findByHerramientasId", query = "SELECT c FROM Conocimientos c WHERE c.conocimientosPK.herramientasId = :herramientasId")})
public class Conocimientos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ConocimientosPK conocimientosPK;
    @JoinColumn(name = "herramientas_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Herramientas herramientas;
    @JoinColumn(name = "ingenieros_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingenieros ingenieros;

    public Conocimientos() {
    }

    public Conocimientos(ConocimientosPK conocimientosPK) {
        this.conocimientosPK = conocimientosPK;
    }

    public Conocimientos(int id, int ingenierosId, int herramientasId) {
        this.conocimientosPK = new ConocimientosPK(id, ingenierosId, herramientasId);
    }

    public ConocimientosPK getConocimientosPK() {
        return conocimientosPK;
    }

    public void setConocimientosPK(ConocimientosPK conocimientosPK) {
        this.conocimientosPK = conocimientosPK;
    }

    public Herramientas getHerramientas() {
        return herramientas;
    }

    public void setHerramientas(Herramientas herramientas) {
        this.herramientas = herramientas;
    }

    public Ingenieros getIngenieros() {
        return ingenieros;
    }

    public void setIngenieros(Ingenieros ingenieros) {
        this.ingenieros = ingenieros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (conocimientosPK != null ? conocimientosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Conocimientos)) {
            return false;
        }
        Conocimientos other = (Conocimientos) object;
        if ((this.conocimientosPK == null && other.conocimientosPK != null) || (this.conocimientosPK != null && !this.conocimientosPK.equals(other.conocimientosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Conocimientos[ conocimientosPK=" + conocimientosPK + " ]";
    }
    
}
