/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class ProyectoHerramientasPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "herramientas_id")
    private int herramientasId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "proyectos_id")
    private int proyectosId;

    public ProyectoHerramientasPK() {
    }

    public ProyectoHerramientasPK(int id, int herramientasId, int proyectosId) {
        this.id = id;
        this.herramientasId = herramientasId;
        this.proyectosId = proyectosId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHerramientasId() {
        return herramientasId;
    }

    public void setHerramientasId(int herramientasId) {
        this.herramientasId = herramientasId;
    }

    public int getProyectosId() {
        return proyectosId;
    }

    public void setProyectosId(int proyectosId) {
        this.proyectosId = proyectosId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) herramientasId;
        hash += (int) proyectosId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyectoHerramientasPK)) {
            return false;
        }
        ProyectoHerramientasPK other = (ProyectoHerramientasPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.herramientasId != other.herramientasId) {
            return false;
        }
        if (this.proyectosId != other.proyectosId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ProyectoHerramientasPK[ id=" + id + ", herramientasId=" + herramientasId + ", proyectosId=" + proyectosId + " ]";
    }
    
}
