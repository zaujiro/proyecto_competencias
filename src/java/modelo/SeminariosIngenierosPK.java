/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class SeminariosIngenierosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ingenieros_id")
    private int ingenierosId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "seminarios_id")
    private int seminariosId;

    public SeminariosIngenierosPK() {
    }

    public SeminariosIngenierosPK(int id, int ingenierosId, int seminariosId) {
        this.id = id;
        this.ingenierosId = ingenierosId;
        this.seminariosId = seminariosId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIngenierosId() {
        return ingenierosId;
    }

    public void setIngenierosId(int ingenierosId) {
        this.ingenierosId = ingenierosId;
    }

    public int getSeminariosId() {
        return seminariosId;
    }

    public void setSeminariosId(int seminariosId) {
        this.seminariosId = seminariosId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) ingenierosId;
        hash += (int) seminariosId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SeminariosIngenierosPK)) {
            return false;
        }
        SeminariosIngenierosPK other = (SeminariosIngenierosPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.ingenierosId != other.ingenierosId) {
            return false;
        }
        if (this.seminariosId != other.seminariosId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.SeminariosIngenierosPK[ id=" + id + ", ingenierosId=" + ingenierosId + ", seminariosId=" + seminariosId + " ]";
    }
    
}
