/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "proyectos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Proyectos.findAll", query = "SELECT p FROM Proyectos p")
    , @NamedQuery(name = "Proyectos.findById", query = "SELECT p FROM Proyectos p WHERE p.proyectosPK.id = :id")
    , @NamedQuery(name = "Proyectos.findByIngenierosId", query = "SELECT p FROM Proyectos p WHERE p.proyectosPK.ingenierosId = :ingenierosId")
    , @NamedQuery(name = "Proyectos.findByCargosId", query = "SELECT p FROM Proyectos p WHERE p.proyectosPK.cargosId = :cargosId")
    , @NamedQuery(name = "Proyectos.findByAreasId", query = "SELECT p FROM Proyectos p WHERE p.proyectosPK.areasId = :areasId")
    , @NamedQuery(name = "Proyectos.findByCodigo", query = "SELECT p FROM Proyectos p WHERE p.codigo = :codigo")
    , @NamedQuery(name = "Proyectos.findByNombre", query = "SELECT p FROM Proyectos p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Proyectos.findByFechaIngreso", query = "SELECT p FROM Proyectos p WHERE p.fechaIngreso = :fechaIngreso")
    , @NamedQuery(name = "Proyectos.findByFechaAsignacion", query = "SELECT p FROM Proyectos p WHERE p.fechaAsignacion = :fechaAsignacion")
    , @NamedQuery(name = "Proyectos.findByFechaLiberacion", query = "SELECT p FROM Proyectos p WHERE p.fechaLiberacion = :fechaLiberacion")
    , @NamedQuery(name = "Proyectos.findByVersion", query = "SELECT p FROM Proyectos p WHERE p.version = :version")
    , @NamedQuery(name = "Proyectos.findByCostoTotal", query = "SELECT p FROM Proyectos p WHERE p.costoTotal = :costoTotal")})
public class Proyectos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProyectosPK proyectosPK;
    @Size(max = 45)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 45)
    @Column(name = "fecha_ingreso")
    private String fechaIngreso;
    @Size(max = 45)
    @Column(name = "fecha_asignacion")
    private String fechaAsignacion;
    @Size(max = 45)
    @Column(name = "fecha_liberacion")
    private String fechaLiberacion;
    @Size(max = 45)
    @Column(name = "version")
    private String version;
    @Size(max = 45)
    @Column(name = "costo_total")
    private String costoTotal;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyectos")
    private List<Colaboradores> colaboradoresList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyectos")
    private List<ProyectoHerramientas> proyectoHerramientasList;
    @JoinColumn(name = "areas_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Areas areas;
    @JoinColumn(name = "cargos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cargos cargos;
    @JoinColumn(name = "ingenieros_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingenieros ingenieros;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyectos")
    private List<Requisitos> requisitosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "proyectos")
    private List<ProyectoFases> proyectoFasesList;

    public Proyectos() {
    }

    public Proyectos(ProyectosPK proyectosPK) {
        this.proyectosPK = proyectosPK;
    }

    public Proyectos(int id, int ingenierosId, int cargosId, int areasId) {
        this.proyectosPK = new ProyectosPK(id, ingenierosId, cargosId, areasId);
    }

    public ProyectosPK getProyectosPK() {
        return proyectosPK;
    }

    public void setProyectosPK(ProyectosPK proyectosPK) {
        this.proyectosPK = proyectosPK;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(String fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public String getFechaLiberacion() {
        return fechaLiberacion;
    }

    public void setFechaLiberacion(String fechaLiberacion) {
        this.fechaLiberacion = fechaLiberacion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getCostoTotal() {
        return costoTotal;
    }

    public void setCostoTotal(String costoTotal) {
        this.costoTotal = costoTotal;
    }

    @XmlTransient
    public List<Colaboradores> getColaboradoresList() {
        return colaboradoresList;
    }

    public void setColaboradoresList(List<Colaboradores> colaboradoresList) {
        this.colaboradoresList = colaboradoresList;
    }

    @XmlTransient
    public List<ProyectoHerramientas> getProyectoHerramientasList() {
        return proyectoHerramientasList;
    }

    public void setProyectoHerramientasList(List<ProyectoHerramientas> proyectoHerramientasList) {
        this.proyectoHerramientasList = proyectoHerramientasList;
    }

    public Areas getAreas() {
        return areas;
    }

    public void setAreas(Areas areas) {
        this.areas = areas;
    }

    public Cargos getCargos() {
        return cargos;
    }

    public void setCargos(Cargos cargos) {
        this.cargos = cargos;
    }

    public Ingenieros getIngenieros() {
        return ingenieros;
    }

    public void setIngenieros(Ingenieros ingenieros) {
        this.ingenieros = ingenieros;
    }

    @XmlTransient
    public List<Requisitos> getRequisitosList() {
        return requisitosList;
    }

    public void setRequisitosList(List<Requisitos> requisitosList) {
        this.requisitosList = requisitosList;
    }

    @XmlTransient
    public List<ProyectoFases> getProyectoFasesList() {
        return proyectoFasesList;
    }

    public void setProyectoFasesList(List<ProyectoFases> proyectoFasesList) {
        this.proyectoFasesList = proyectoFasesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyectosPK != null ? proyectosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Proyectos)) {
            return false;
        }
        Proyectos other = (Proyectos) object;
        if ((this.proyectosPK == null && other.proyectosPK != null) || (this.proyectosPK != null && !this.proyectosPK.equals(other.proyectosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Proyectos[ proyectosPK=" + proyectosPK + " ]";
    }
    
}
