/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class GradosIngenieroPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "grados_academicos_id")
    private int gradosAcademicosId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ingenieros_id")
    private int ingenierosId;

    public GradosIngenieroPK() {
    }

    public GradosIngenieroPK(int id, int gradosAcademicosId, int ingenierosId) {
        this.id = id;
        this.gradosAcademicosId = gradosAcademicosId;
        this.ingenierosId = ingenierosId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGradosAcademicosId() {
        return gradosAcademicosId;
    }

    public void setGradosAcademicosId(int gradosAcademicosId) {
        this.gradosAcademicosId = gradosAcademicosId;
    }

    public int getIngenierosId() {
        return ingenierosId;
    }

    public void setIngenierosId(int ingenierosId) {
        this.ingenierosId = ingenierosId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) gradosAcademicosId;
        hash += (int) ingenierosId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GradosIngenieroPK)) {
            return false;
        }
        GradosIngenieroPK other = (GradosIngenieroPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.gradosAcademicosId != other.gradosAcademicosId) {
            return false;
        }
        if (this.ingenierosId != other.ingenierosId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.GradosIngenieroPK[ id=" + id + ", gradosAcademicosId=" + gradosAcademicosId + ", ingenierosId=" + ingenierosId + " ]";
    }
    
}
