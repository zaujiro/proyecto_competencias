/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "seminarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Seminarios.findAll", query = "SELECT s FROM Seminarios s")
    , @NamedQuery(name = "Seminarios.findById", query = "SELECT s FROM Seminarios s WHERE s.id = :id")
    , @NamedQuery(name = "Seminarios.findByNombre", query = "SELECT s FROM Seminarios s WHERE s.nombre = :nombre")
    , @NamedQuery(name = "Seminarios.findByLugar", query = "SELECT s FROM Seminarios s WHERE s.lugar = :lugar")})
public class Seminarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 45)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 45)
    @Column(name = "lugar")
    private String lugar;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "seminarios")
    private List<SeminariosIngenieros> seminariosIngenierosList;

    public Seminarios() {
    }

    public Seminarios(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    @XmlTransient
    public List<SeminariosIngenieros> getSeminariosIngenierosList() {
        return seminariosIngenierosList;
    }

    public void setSeminariosIngenierosList(List<SeminariosIngenieros> seminariosIngenierosList) {
        this.seminariosIngenierosList = seminariosIngenierosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Seminarios)) {
            return false;
        }
        Seminarios other = (Seminarios) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Seminarios[ id=" + id + " ]";
    }
    
}
