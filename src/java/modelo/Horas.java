/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "horas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Horas.findAll", query = "SELECT h FROM Horas h")
    , @NamedQuery(name = "Horas.findById", query = "SELECT h FROM Horas h WHERE h.horasPK.id = :id")
    , @NamedQuery(name = "Horas.findByColaboradoresId", query = "SELECT h FROM Horas h WHERE h.horasPK.colaboradoresId = :colaboradoresId")
    , @NamedQuery(name = "Horas.findByCantidad", query = "SELECT h FROM Horas h WHERE h.cantidad = :cantidad")
    , @NamedQuery(name = "Horas.findByFecha", query = "SELECT h FROM Horas h WHERE h.fecha = :fecha")})
public class Horas implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected HorasPK horasPK;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cantidad")
    private Float cantidad;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @JoinColumn(name = "colaboradores_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Colaboradores colaboradores;

    public Horas() {
    }

    public Horas(HorasPK horasPK) {
        this.horasPK = horasPK;
    }

    public Horas(int id, int colaboradoresId) {
        this.horasPK = new HorasPK(id, colaboradoresId);
    }

    public HorasPK getHorasPK() {
        return horasPK;
    }

    public void setHorasPK(HorasPK horasPK) {
        this.horasPK = horasPK;
    }

    public Float getCantidad() {
        return cantidad;
    }

    public void setCantidad(Float cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Colaboradores getColaboradores() {
        return colaboradores;
    }

    public void setColaboradores(Colaboradores colaboradores) {
        this.colaboradores = colaboradores;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (horasPK != null ? horasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Horas)) {
            return false;
        }
        Horas other = (Horas) object;
        if ((this.horasPK == null && other.horasPK != null) || (this.horasPK != null && !this.horasPK.equals(other.horasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Horas[ horasPK=" + horasPK + " ]";
    }
    
}
