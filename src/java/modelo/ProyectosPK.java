/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class ProyectosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ingenieros_id")
    private int ingenierosId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cargos_id")
    private int cargosId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "areas_id")
    private int areasId;

    public ProyectosPK() {
    }

    public ProyectosPK(int id, int ingenierosId, int cargosId, int areasId) {
        this.id = id;
        this.ingenierosId = ingenierosId;
        this.cargosId = cargosId;
        this.areasId = areasId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIngenierosId() {
        return ingenierosId;
    }

    public void setIngenierosId(int ingenierosId) {
        this.ingenierosId = ingenierosId;
    }

    public int getCargosId() {
        return cargosId;
    }

    public void setCargosId(int cargosId) {
        this.cargosId = cargosId;
    }

    public int getAreasId() {
        return areasId;
    }

    public void setAreasId(int areasId) {
        this.areasId = areasId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) ingenierosId;
        hash += (int) cargosId;
        hash += (int) areasId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyectosPK)) {
            return false;
        }
        ProyectosPK other = (ProyectosPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.ingenierosId != other.ingenierosId) {
            return false;
        }
        if (this.cargosId != other.cargosId) {
            return false;
        }
        if (this.areasId != other.areasId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ProyectosPK[ id=" + id + ", ingenierosId=" + ingenierosId + ", cargosId=" + cargosId + ", areasId=" + areasId + " ]";
    }
    
}
