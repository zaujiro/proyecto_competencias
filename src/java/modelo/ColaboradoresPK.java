/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class ColaboradoresPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "proyectos_id")
    private int proyectosId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ingenieros_id")
    private int ingenierosId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cargos_id")
    private int cargosId;

    public ColaboradoresPK() {
    }

    public ColaboradoresPK(int id, int proyectosId, int ingenierosId, int cargosId) {
        this.id = id;
        this.proyectosId = proyectosId;
        this.ingenierosId = ingenierosId;
        this.cargosId = cargosId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProyectosId() {
        return proyectosId;
    }

    public void setProyectosId(int proyectosId) {
        this.proyectosId = proyectosId;
    }

    public int getIngenierosId() {
        return ingenierosId;
    }

    public void setIngenierosId(int ingenierosId) {
        this.ingenierosId = ingenierosId;
    }

    public int getCargosId() {
        return cargosId;
    }

    public void setCargosId(int cargosId) {
        this.cargosId = cargosId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) proyectosId;
        hash += (int) ingenierosId;
        hash += (int) cargosId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ColaboradoresPK)) {
            return false;
        }
        ColaboradoresPK other = (ColaboradoresPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.proyectosId != other.proyectosId) {
            return false;
        }
        if (this.ingenierosId != other.ingenierosId) {
            return false;
        }
        if (this.cargosId != other.cargosId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ColaboradoresPK[ id=" + id + ", proyectosId=" + proyectosId + ", ingenierosId=" + ingenierosId + ", cargosId=" + cargosId + " ]";
    }
    
}
