/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class SolicitudesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "requisitos_id")
    private int requisitosId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuarios_id_solicitante")
    private int usuariosIdSolicitante;

    public SolicitudesPK() {
    }

    public SolicitudesPK(int id, int requisitosId, int usuariosIdSolicitante) {
        this.id = id;
        this.requisitosId = requisitosId;
        this.usuariosIdSolicitante = usuariosIdSolicitante;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRequisitosId() {
        return requisitosId;
    }

    public void setRequisitosId(int requisitosId) {
        this.requisitosId = requisitosId;
    }

    public int getUsuariosIdSolicitante() {
        return usuariosIdSolicitante;
    }

    public void setUsuariosIdSolicitante(int usuariosIdSolicitante) {
        this.usuariosIdSolicitante = usuariosIdSolicitante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) requisitosId;
        hash += (int) usuariosIdSolicitante;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolicitudesPK)) {
            return false;
        }
        SolicitudesPK other = (SolicitudesPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.requisitosId != other.requisitosId) {
            return false;
        }
        if (this.usuariosIdSolicitante != other.usuariosIdSolicitante) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.SolicitudesPK[ id=" + id + ", requisitosId=" + requisitosId + ", usuariosIdSolicitante=" + usuariosIdSolicitante + " ]";
    }
    
}
