/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "colaboradores")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Colaboradores.findAll", query = "SELECT c FROM Colaboradores c")
    , @NamedQuery(name = "Colaboradores.findById", query = "SELECT c FROM Colaboradores c WHERE c.colaboradoresPK.id = :id")
    , @NamedQuery(name = "Colaboradores.findByProyectosId", query = "SELECT c FROM Colaboradores c WHERE c.colaboradoresPK.proyectosId = :proyectosId")
    , @NamedQuery(name = "Colaboradores.findByIngenierosId", query = "SELECT c FROM Colaboradores c WHERE c.colaboradoresPK.ingenierosId = :ingenierosId")
    , @NamedQuery(name = "Colaboradores.findByCargosId", query = "SELECT c FROM Colaboradores c WHERE c.colaboradoresPK.cargosId = :cargosId")
    , @NamedQuery(name = "Colaboradores.findByFechaInicio", query = "SELECT c FROM Colaboradores c WHERE c.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "Colaboradores.findByFechaFin", query = "SELECT c FROM Colaboradores c WHERE c.fechaFin = :fechaFin")})
public class Colaboradores implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ColaboradoresPK colaboradoresPK;
    @Size(max = 45)
    @Column(name = "fecha_inicio")
    private String fechaInicio;
    @Size(max = 45)
    @Column(name = "fecha_fin")
    private String fechaFin;
    @JoinColumn(name = "cargos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cargos cargos;
    @JoinColumn(name = "ingenieros_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingenieros ingenieros;
    @JoinColumn(name = "proyectos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proyectos proyectos;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "colaboradores")
    private List<Horas> horasList;

    public Colaboradores() {
    }

    public Colaboradores(ColaboradoresPK colaboradoresPK) {
        this.colaboradoresPK = colaboradoresPK;
    }

    public Colaboradores(int id, int proyectosId, int ingenierosId, int cargosId) {
        this.colaboradoresPK = new ColaboradoresPK(id, proyectosId, ingenierosId, cargosId);
    }

    public ColaboradoresPK getColaboradoresPK() {
        return colaboradoresPK;
    }

    public void setColaboradoresPK(ColaboradoresPK colaboradoresPK) {
        this.colaboradoresPK = colaboradoresPK;
    }

    public String getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(String fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(String fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Cargos getCargos() {
        return cargos;
    }

    public void setCargos(Cargos cargos) {
        this.cargos = cargos;
    }

    public Ingenieros getIngenieros() {
        return ingenieros;
    }

    public void setIngenieros(Ingenieros ingenieros) {
        this.ingenieros = ingenieros;
    }

    public Proyectos getProyectos() {
        return proyectos;
    }

    public void setProyectos(Proyectos proyectos) {
        this.proyectos = proyectos;
    }

    @XmlTransient
    public List<Horas> getHorasList() {
        return horasList;
    }

    public void setHorasList(List<Horas> horasList) {
        this.horasList = horasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (colaboradoresPK != null ? colaboradoresPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Colaboradores)) {
            return false;
        }
        Colaboradores other = (Colaboradores) object;
        if ((this.colaboradoresPK == null && other.colaboradoresPK != null) || (this.colaboradoresPK != null && !this.colaboradoresPK.equals(other.colaboradoresPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Colaboradores[ colaboradoresPK=" + colaboradoresPK + " ]";
    }
    
}
