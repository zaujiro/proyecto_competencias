/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "proyecto_herramientas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProyectoHerramientas.findAll", query = "SELECT p FROM ProyectoHerramientas p")
    , @NamedQuery(name = "ProyectoHerramientas.findById", query = "SELECT p FROM ProyectoHerramientas p WHERE p.proyectoHerramientasPK.id = :id")
    , @NamedQuery(name = "ProyectoHerramientas.findByHerramientasId", query = "SELECT p FROM ProyectoHerramientas p WHERE p.proyectoHerramientasPK.herramientasId = :herramientasId")
    , @NamedQuery(name = "ProyectoHerramientas.findByProyectosId", query = "SELECT p FROM ProyectoHerramientas p WHERE p.proyectoHerramientasPK.proyectosId = :proyectosId")})
public class ProyectoHerramientas implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProyectoHerramientasPK proyectoHerramientasPK;
    @JoinColumn(name = "herramientas_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Herramientas herramientas;
    @JoinColumn(name = "proyectos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proyectos proyectos;

    public ProyectoHerramientas() {
    }

    public ProyectoHerramientas(ProyectoHerramientasPK proyectoHerramientasPK) {
        this.proyectoHerramientasPK = proyectoHerramientasPK;
    }

    public ProyectoHerramientas(int id, int herramientasId, int proyectosId) {
        this.proyectoHerramientasPK = new ProyectoHerramientasPK(id, herramientasId, proyectosId);
    }

    public ProyectoHerramientasPK getProyectoHerramientasPK() {
        return proyectoHerramientasPK;
    }

    public void setProyectoHerramientasPK(ProyectoHerramientasPK proyectoHerramientasPK) {
        this.proyectoHerramientasPK = proyectoHerramientasPK;
    }

    public Herramientas getHerramientas() {
        return herramientas;
    }

    public void setHerramientas(Herramientas herramientas) {
        this.herramientas = herramientas;
    }

    public Proyectos getProyectos() {
        return proyectos;
    }

    public void setProyectos(Proyectos proyectos) {
        this.proyectos = proyectos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyectoHerramientasPK != null ? proyectoHerramientasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyectoHerramientas)) {
            return false;
        }
        ProyectoHerramientas other = (ProyectoHerramientas) object;
        if ((this.proyectoHerramientasPK == null && other.proyectoHerramientasPK != null) || (this.proyectoHerramientasPK != null && !this.proyectoHerramientasPK.equals(other.proyectoHerramientasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ProyectoHerramientas[ proyectoHerramientasPK=" + proyectoHerramientasPK + " ]";
    }
    
}
