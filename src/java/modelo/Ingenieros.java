/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "ingenieros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ingenieros.findAll", query = "SELECT i FROM Ingenieros i")
    , @NamedQuery(name = "Ingenieros.findById", query = "SELECT i FROM Ingenieros i WHERE i.ingenierosPK.id = :id")
    , @NamedQuery(name = "Ingenieros.findByCargosId", query = "SELECT i FROM Ingenieros i WHERE i.ingenierosPK.cargosId = :cargosId")
    , @NamedQuery(name = "Ingenieros.findByNombres", query = "SELECT i FROM Ingenieros i WHERE i.nombres = :nombres")
    , @NamedQuery(name = "Ingenieros.findByApellidos", query = "SELECT i FROM Ingenieros i WHERE i.apellidos = :apellidos")
    , @NamedQuery(name = "Ingenieros.findByCedula", query = "SELECT i FROM Ingenieros i WHERE i.cedula = :cedula")
    , @NamedQuery(name = "Ingenieros.findByEmail", query = "SELECT i FROM Ingenieros i WHERE i.email = :email")
    , @NamedQuery(name = "Ingenieros.findByTelefono", query = "SELECT i FROM Ingenieros i WHERE i.telefono = :telefono")
    , @NamedQuery(name = "Ingenieros.findByMovil", query = "SELECT i FROM Ingenieros i WHERE i.movil = :movil")
    , @NamedQuery(name = "Ingenieros.findByFechaNacimiento", query = "SELECT i FROM Ingenieros i WHERE i.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Ingenieros.findByEdad", query = "SELECT i FROM Ingenieros i WHERE i.edad = :edad")
    , @NamedQuery(name = "Ingenieros.findBySexo", query = "SELECT i FROM Ingenieros i WHERE i.sexo = :sexo")
    , @NamedQuery(name = "Ingenieros.findByFechaIngreso", query = "SELECT i FROM Ingenieros i WHERE i.fechaIngreso = :fechaIngreso")})
public class Ingenieros implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected IngenierosPK ingenierosPK;
    @Size(max = 45)
    @Column(name = "nombres")
    private String nombres;
    @Size(max = 45)
    @Column(name = "apellidos")
    private String apellidos;
    @Size(max = 45)
    @Column(name = "cedula")
    private String cedula;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 45)
    @Column(name = "email")
    private String email;
    @Size(max = 45)
    @Column(name = "telefono")
    private String telefono;
    @Size(max = 45)
    @Column(name = "movil")
    private String movil;
    @Size(max = 45)
    @Column(name = "fecha_nacimiento")
    private String fechaNacimiento;
    @Size(max = 45)
    @Column(name = "edad")
    private String edad;
    @Size(max = 45)
    @Column(name = "sexo")
    private String sexo;
    @Size(max = 45)
    @Column(name = "fecha_ingreso")
    private String fechaIngreso;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingenieros")
    private List<Colaboradores> colaboradoresList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingenieros")
    private List<Trabajos> trabajosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingenieros")
    private List<Proyectos> proyectosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingenieros")
    private List<SeminariosIngenieros> seminariosIngenierosList;
    @JoinColumn(name = "cargos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cargos cargos;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingenieros")
    private List<Conocimientos> conocimientosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "ingenieros")
    private List<GradosIngeniero> gradosIngenieroList;

    public Ingenieros() {
    }

    public Ingenieros(IngenierosPK ingenierosPK) {
        this.ingenierosPK = ingenierosPK;
    }

    public Ingenieros(int id, int cargosId) {
        this.ingenierosPK = new IngenierosPK(id, cargosId);
    }

    public IngenierosPK getIngenierosPK() {
        return ingenierosPK;
    }

    public void setIngenierosPK(IngenierosPK ingenierosPK) {
        this.ingenierosPK = ingenierosPK;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getMovil() {
        return movil;
    }

    public void setMovil(String movil) {
        this.movil = movil;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    @XmlTransient
    public List<Colaboradores> getColaboradoresList() {
        return colaboradoresList;
    }

    public void setColaboradoresList(List<Colaboradores> colaboradoresList) {
        this.colaboradoresList = colaboradoresList;
    }

    @XmlTransient
    public List<Trabajos> getTrabajosList() {
        return trabajosList;
    }

    public void setTrabajosList(List<Trabajos> trabajosList) {
        this.trabajosList = trabajosList;
    }

    @XmlTransient
    public List<Proyectos> getProyectosList() {
        return proyectosList;
    }

    public void setProyectosList(List<Proyectos> proyectosList) {
        this.proyectosList = proyectosList;
    }

    @XmlTransient
    public List<SeminariosIngenieros> getSeminariosIngenierosList() {
        return seminariosIngenierosList;
    }

    public void setSeminariosIngenierosList(List<SeminariosIngenieros> seminariosIngenierosList) {
        this.seminariosIngenierosList = seminariosIngenierosList;
    }

    public Cargos getCargos() {
        return cargos;
    }

    public void setCargos(Cargos cargos) {
        this.cargos = cargos;
    }

    @XmlTransient
    public List<Conocimientos> getConocimientosList() {
        return conocimientosList;
    }

    public void setConocimientosList(List<Conocimientos> conocimientosList) {
        this.conocimientosList = conocimientosList;
    }

    @XmlTransient
    public List<GradosIngeniero> getGradosIngenieroList() {
        return gradosIngenieroList;
    }

    public void setGradosIngenieroList(List<GradosIngeniero> gradosIngenieroList) {
        this.gradosIngenieroList = gradosIngenieroList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ingenierosPK != null ? ingenierosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ingenieros)) {
            return false;
        }
        Ingenieros other = (Ingenieros) object;
        if ((this.ingenierosPK == null && other.ingenierosPK != null) || (this.ingenierosPK != null && !this.ingenierosPK.equals(other.ingenierosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Ingenieros[ ingenierosPK=" + ingenierosPK + " ]";
    }
    
}
