/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "grados_ingeniero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "GradosIngeniero.findAll", query = "SELECT g FROM GradosIngeniero g")
    , @NamedQuery(name = "GradosIngeniero.findById", query = "SELECT g FROM GradosIngeniero g WHERE g.gradosIngenieroPK.id = :id")
    , @NamedQuery(name = "GradosIngeniero.findByGradosAcademicosId", query = "SELECT g FROM GradosIngeniero g WHERE g.gradosIngenieroPK.gradosAcademicosId = :gradosAcademicosId")
    , @NamedQuery(name = "GradosIngeniero.findByIngenierosId", query = "SELECT g FROM GradosIngeniero g WHERE g.gradosIngenieroPK.ingenierosId = :ingenierosId")
    , @NamedQuery(name = "GradosIngeniero.findByUniversidad", query = "SELECT g FROM GradosIngeniero g WHERE g.universidad = :universidad")
    , @NamedQuery(name = "GradosIngeniero.findByTitulo", query = "SELECT g FROM GradosIngeniero g WHERE g.titulo = :titulo")
    , @NamedQuery(name = "GradosIngeniero.findByFechaTitulacion", query = "SELECT g FROM GradosIngeniero g WHERE g.fechaTitulacion = :fechaTitulacion")})
public class GradosIngeniero implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected GradosIngenieroPK gradosIngenieroPK;
    @Size(max = 45)
    @Column(name = "universidad")
    private String universidad;
    @Size(max = 45)
    @Column(name = "titulo")
    private String titulo;
    @Size(max = 45)
    @Column(name = "fecha_titulacion")
    private String fechaTitulacion;
    @JoinColumn(name = "grados_academicos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private GradosAcademicos gradosAcademicos;
    @JoinColumn(name = "ingenieros_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingenieros ingenieros;

    public GradosIngeniero() {
    }

    public GradosIngeniero(GradosIngenieroPK gradosIngenieroPK) {
        this.gradosIngenieroPK = gradosIngenieroPK;
    }

    public GradosIngeniero(int id, int gradosAcademicosId, int ingenierosId) {
        this.gradosIngenieroPK = new GradosIngenieroPK(id, gradosAcademicosId, ingenierosId);
    }

    public GradosIngenieroPK getGradosIngenieroPK() {
        return gradosIngenieroPK;
    }

    public void setGradosIngenieroPK(GradosIngenieroPK gradosIngenieroPK) {
        this.gradosIngenieroPK = gradosIngenieroPK;
    }

    public String getUniversidad() {
        return universidad;
    }

    public void setUniversidad(String universidad) {
        this.universidad = universidad;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFechaTitulacion() {
        return fechaTitulacion;
    }

    public void setFechaTitulacion(String fechaTitulacion) {
        this.fechaTitulacion = fechaTitulacion;
    }

    public GradosAcademicos getGradosAcademicos() {
        return gradosAcademicos;
    }

    public void setGradosAcademicos(GradosAcademicos gradosAcademicos) {
        this.gradosAcademicos = gradosAcademicos;
    }

    public Ingenieros getIngenieros() {
        return ingenieros;
    }

    public void setIngenieros(Ingenieros ingenieros) {
        this.ingenieros = ingenieros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (gradosIngenieroPK != null ? gradosIngenieroPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof GradosIngeniero)) {
            return false;
        }
        GradosIngeniero other = (GradosIngeniero) object;
        if ((this.gradosIngenieroPK == null && other.gradosIngenieroPK != null) || (this.gradosIngenieroPK != null && !this.gradosIngenieroPK.equals(other.gradosIngenieroPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.GradosIngeniero[ gradosIngenieroPK=" + gradosIngenieroPK + " ]";
    }
    
}
