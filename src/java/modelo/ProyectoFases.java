/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "proyecto_fases")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ProyectoFases.findAll", query = "SELECT p FROM ProyectoFases p")
    , @NamedQuery(name = "ProyectoFases.findById", query = "SELECT p FROM ProyectoFases p WHERE p.proyectoFasesPK.id = :id")
    , @NamedQuery(name = "ProyectoFases.findByProyectosId", query = "SELECT p FROM ProyectoFases p WHERE p.proyectoFasesPK.proyectosId = :proyectosId")
    , @NamedQuery(name = "ProyectoFases.findByFasesId", query = "SELECT p FROM ProyectoFases p WHERE p.proyectoFasesPK.fasesId = :fasesId")
    , @NamedQuery(name = "ProyectoFases.findByFecha", query = "SELECT p FROM ProyectoFases p WHERE p.fecha = :fecha")})
public class ProyectoFases implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ProyectoFasesPK proyectoFasesPK;
    @Size(max = 45)
    @Column(name = "fecha")
    private String fecha;
    @JoinColumn(name = "fases_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Fases fases;
    @JoinColumn(name = "proyectos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Proyectos proyectos;

    public ProyectoFases() {
    }

    public ProyectoFases(ProyectoFasesPK proyectoFasesPK) {
        this.proyectoFasesPK = proyectoFasesPK;
    }

    public ProyectoFases(int id, int proyectosId, int fasesId) {
        this.proyectoFasesPK = new ProyectoFasesPK(id, proyectosId, fasesId);
    }

    public ProyectoFasesPK getProyectoFasesPK() {
        return proyectoFasesPK;
    }

    public void setProyectoFasesPK(ProyectoFasesPK proyectoFasesPK) {
        this.proyectoFasesPK = proyectoFasesPK;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Fases getFases() {
        return fases;
    }

    public void setFases(Fases fases) {
        this.fases = fases;
    }

    public Proyectos getProyectos() {
        return proyectos;
    }

    public void setProyectos(Proyectos proyectos) {
        this.proyectos = proyectos;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (proyectoFasesPK != null ? proyectoFasesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyectoFases)) {
            return false;
        }
        ProyectoFases other = (ProyectoFases) object;
        if ((this.proyectoFasesPK == null && other.proyectoFasesPK != null) || (this.proyectoFasesPK != null && !this.proyectoFasesPK.equals(other.proyectoFasesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ProyectoFases[ proyectoFasesPK=" + proyectoFasesPK + " ]";
    }
    
}
