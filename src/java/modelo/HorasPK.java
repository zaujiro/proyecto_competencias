/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class HorasPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "colaboradores_id")
    private int colaboradoresId;

    public HorasPK() {
    }

    public HorasPK(int id, int colaboradoresId) {
        this.id = id;
        this.colaboradoresId = colaboradoresId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getColaboradoresId() {
        return colaboradoresId;
    }

    public void setColaboradoresId(int colaboradoresId) {
        this.colaboradoresId = colaboradoresId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) colaboradoresId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof HorasPK)) {
            return false;
        }
        HorasPK other = (HorasPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.colaboradoresId != other.colaboradoresId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.HorasPK[ id=" + id + ", colaboradoresId=" + colaboradoresId + " ]";
    }
    
}
