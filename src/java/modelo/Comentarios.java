/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "comentarios")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Comentarios.findAll", query = "SELECT c FROM Comentarios c")
    , @NamedQuery(name = "Comentarios.findById", query = "SELECT c FROM Comentarios c WHERE c.comentariosPK.id = :id")
    , @NamedQuery(name = "Comentarios.findBySolicitudesId", query = "SELECT c FROM Comentarios c WHERE c.comentariosPK.solicitudesId = :solicitudesId")
    , @NamedQuery(name = "Comentarios.findByUsuariosId", query = "SELECT c FROM Comentarios c WHERE c.comentariosPK.usuariosId = :usuariosId")
    , @NamedQuery(name = "Comentarios.findByComentario", query = "SELECT c FROM Comentarios c WHERE c.comentario = :comentario")})
public class Comentarios implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected ComentariosPK comentariosPK;
    @Size(max = 45)
    @Column(name = "comentario")
    private String comentario;
    @JoinColumn(name = "solicitudes_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Solicitudes solicitudes;
    @JoinColumn(name = "usuarios_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuarios usuarios;

    public Comentarios() {
    }

    public Comentarios(ComentariosPK comentariosPK) {
        this.comentariosPK = comentariosPK;
    }

    public Comentarios(int id, int solicitudesId, int usuariosId) {
        this.comentariosPK = new ComentariosPK(id, solicitudesId, usuariosId);
    }

    public ComentariosPK getComentariosPK() {
        return comentariosPK;
    }

    public void setComentariosPK(ComentariosPK comentariosPK) {
        this.comentariosPK = comentariosPK;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Solicitudes getSolicitudes() {
        return solicitudes;
    }

    public void setSolicitudes(Solicitudes solicitudes) {
        this.solicitudes = solicitudes;
    }

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (comentariosPK != null ? comentariosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Comentarios)) {
            return false;
        }
        Comentarios other = (Comentarios) object;
        if ((this.comentariosPK == null && other.comentariosPK != null) || (this.comentariosPK != null && !this.comentariosPK.equals(other.comentariosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Comentarios[ comentariosPK=" + comentariosPK + " ]";
    }
    
}
