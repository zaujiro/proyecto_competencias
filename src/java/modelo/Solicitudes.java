/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "solicitudes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Solicitudes.findAll", query = "SELECT s FROM Solicitudes s")
    , @NamedQuery(name = "Solicitudes.findById", query = "SELECT s FROM Solicitudes s WHERE s.solicitudesPK.id = :id")
    , @NamedQuery(name = "Solicitudes.findByRequisitosId", query = "SELECT s FROM Solicitudes s WHERE s.solicitudesPK.requisitosId = :requisitosId")
    , @NamedQuery(name = "Solicitudes.findByUsuariosIdSolicitante", query = "SELECT s FROM Solicitudes s WHERE s.solicitudesPK.usuariosIdSolicitante = :usuariosIdSolicitante")
    , @NamedQuery(name = "Solicitudes.findByTipo", query = "SELECT s FROM Solicitudes s WHERE s.tipo = :tipo")
    , @NamedQuery(name = "Solicitudes.findByTitulo", query = "SELECT s FROM Solicitudes s WHERE s.titulo = :titulo")
    , @NamedQuery(name = "Solicitudes.findByFecha", query = "SELECT s FROM Solicitudes s WHERE s.fecha = :fecha")
    , @NamedQuery(name = "Solicitudes.findByOrigen", query = "SELECT s FROM Solicitudes s WHERE s.origen = :origen")
    , @NamedQuery(name = "Solicitudes.findByEstado", query = "SELECT s FROM Solicitudes s WHERE s.estado = :estado")
    , @NamedQuery(name = "Solicitudes.findByPrioridadSolicitante", query = "SELECT s FROM Solicitudes s WHERE s.prioridadSolicitante = :prioridadSolicitante")
    , @NamedQuery(name = "Solicitudes.findByPrioridadRealizacion", query = "SELECT s FROM Solicitudes s WHERE s.prioridadRealizacion = :prioridadRealizacion")
    , @NamedQuery(name = "Solicitudes.findByFechaActualizacion", query = "SELECT s FROM Solicitudes s WHERE s.fechaActualizacion = :fechaActualizacion")
    , @NamedQuery(name = "Solicitudes.findByRelease", query = "SELECT s FROM Solicitudes s WHERE s.release = :release")
    , @NamedQuery(name = "Solicitudes.findByEsfuerzo", query = "SELECT s FROM Solicitudes s WHERE s.esfuerzo = :esfuerzo")
    , @NamedQuery(name = "Solicitudes.findByDescripcion", query = "SELECT s FROM Solicitudes s WHERE s.descripcion = :descripcion")})
public class Solicitudes implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected SolicitudesPK solicitudesPK;
    @Size(max = 9)
    @Column(name = "tipo")
    private String tipo;
    @Size(max = 100)
    @Column(name = "titulo")
    private String titulo;
    @Column(name = "fecha")
    @Temporal(TemporalType.DATE)
    private Date fecha;
    @Size(max = 255)
    @Column(name = "origen")
    private String origen;
    @Size(max = 11)
    @Column(name = "estado")
    private String estado;
    @Size(max = 5)
    @Column(name = "prioridad_solicitante")
    private String prioridadSolicitante;
    @Size(max = 5)
    @Column(name = "prioridad_realizacion")
    private String prioridadRealizacion;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.DATE)
    private Date fechaActualizacion;
    @Size(max = 100)
    @Column(name = "release")
    private String release;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "esfuerzo")
    private Float esfuerzo;
    @Size(max = 255)
    @Column(name = "descripcion")
    private String descripcion;
    @JoinColumn(name = "requisitos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Requisitos requisitos;
    @JoinColumn(name = "usuarios_id_solicitante", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuarios usuarios;
    @JoinColumn(name = "usuarios_id_verificador", referencedColumnName = "id")
    @ManyToOne
    private Usuarios usuariosIdVerificador;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "solicitudes")
    private List<Comentarios> comentariosList;

    public Solicitudes() {
    }

    public Solicitudes(SolicitudesPK solicitudesPK) {
        this.solicitudesPK = solicitudesPK;
    }

    public Solicitudes(int id, int requisitosId, int usuariosIdSolicitante) {
        this.solicitudesPK = new SolicitudesPK(id, requisitosId, usuariosIdSolicitante);
    }

    public SolicitudesPK getSolicitudesPK() {
        return solicitudesPK;
    }

    public void setSolicitudesPK(SolicitudesPK solicitudesPK) {
        this.solicitudesPK = solicitudesPK;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getOrigen() {
        return origen;
    }

    public void setOrigen(String origen) {
        this.origen = origen;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPrioridadSolicitante() {
        return prioridadSolicitante;
    }

    public void setPrioridadSolicitante(String prioridadSolicitante) {
        this.prioridadSolicitante = prioridadSolicitante;
    }

    public String getPrioridadRealizacion() {
        return prioridadRealizacion;
    }

    public void setPrioridadRealizacion(String prioridadRealizacion) {
        this.prioridadRealizacion = prioridadRealizacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public String getRelease() {
        return release;
    }

    public void setRelease(String release) {
        this.release = release;
    }

    public Float getEsfuerzo() {
        return esfuerzo;
    }

    public void setEsfuerzo(Float esfuerzo) {
        this.esfuerzo = esfuerzo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Requisitos getRequisitos() {
        return requisitos;
    }

    public void setRequisitos(Requisitos requisitos) {
        this.requisitos = requisitos;
    }

    public Usuarios getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Usuarios usuarios) {
        this.usuarios = usuarios;
    }

    public Usuarios getUsuariosIdVerificador() {
        return usuariosIdVerificador;
    }

    public void setUsuariosIdVerificador(Usuarios usuariosIdVerificador) {
        this.usuariosIdVerificador = usuariosIdVerificador;
    }

    @XmlTransient
    public List<Comentarios> getComentariosList() {
        return comentariosList;
    }

    public void setComentariosList(List<Comentarios> comentariosList) {
        this.comentariosList = comentariosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (solicitudesPK != null ? solicitudesPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Solicitudes)) {
            return false;
        }
        Solicitudes other = (Solicitudes) object;
        if ((this.solicitudesPK == null && other.solicitudesPK != null) || (this.solicitudesPK != null && !this.solicitudesPK.equals(other.solicitudesPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Solicitudes[ solicitudesPK=" + solicitudesPK + " ]";
    }
    
}
