/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class ComentariosPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "solicitudes_id")
    private int solicitudesId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "usuarios_id")
    private int usuariosId;

    public ComentariosPK() {
    }

    public ComentariosPK(int id, int solicitudesId, int usuariosId) {
        this.id = id;
        this.solicitudesId = solicitudesId;
        this.usuariosId = usuariosId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSolicitudesId() {
        return solicitudesId;
    }

    public void setSolicitudesId(int solicitudesId) {
        this.solicitudesId = solicitudesId;
    }

    public int getUsuariosId() {
        return usuariosId;
    }

    public void setUsuariosId(int usuariosId) {
        this.usuariosId = usuariosId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) solicitudesId;
        hash += (int) usuariosId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ComentariosPK)) {
            return false;
        }
        ComentariosPK other = (ComentariosPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.solicitudesId != other.solicitudesId) {
            return false;
        }
        if (this.usuariosId != other.usuariosId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ComentariosPK[ id=" + id + ", solicitudesId=" + solicitudesId + ", usuariosId=" + usuariosId + " ]";
    }
    
}
