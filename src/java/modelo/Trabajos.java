/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Sistemas
 */
@Entity
@Table(name = "trabajos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Trabajos.findAll", query = "SELECT t FROM Trabajos t")
    , @NamedQuery(name = "Trabajos.findById", query = "SELECT t FROM Trabajos t WHERE t.trabajosPK.id = :id")
    , @NamedQuery(name = "Trabajos.findByIngenierosId", query = "SELECT t FROM Trabajos t WHERE t.trabajosPK.ingenierosId = :ingenierosId")
    , @NamedQuery(name = "Trabajos.findByCargosId", query = "SELECT t FROM Trabajos t WHERE t.trabajosPK.cargosId = :cargosId")
    , @NamedQuery(name = "Trabajos.findByValor", query = "SELECT t FROM Trabajos t WHERE t.valor = :valor")})
public class Trabajos implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected TrabajosPK trabajosPK;
    @Size(max = 45)
    @Column(name = "valor")
    private String valor;
    @JoinColumn(name = "cargos_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Cargos cargos;
    @JoinColumn(name = "ingenieros_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Ingenieros ingenieros;

    public Trabajos() {
    }

    public Trabajos(TrabajosPK trabajosPK) {
        this.trabajosPK = trabajosPK;
    }

    public Trabajos(int id, int ingenierosId, int cargosId) {
        this.trabajosPK = new TrabajosPK(id, ingenierosId, cargosId);
    }

    public TrabajosPK getTrabajosPK() {
        return trabajosPK;
    }

    public void setTrabajosPK(TrabajosPK trabajosPK) {
        this.trabajosPK = trabajosPK;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Cargos getCargos() {
        return cargos;
    }

    public void setCargos(Cargos cargos) {
        this.cargos = cargos;
    }

    public Ingenieros getIngenieros() {
        return ingenieros;
    }

    public void setIngenieros(Ingenieros ingenieros) {
        this.ingenieros = ingenieros;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (trabajosPK != null ? trabajosPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Trabajos)) {
            return false;
        }
        Trabajos other = (Trabajos) object;
        if ((this.trabajosPK == null && other.trabajosPK != null) || (this.trabajosPK != null && !this.trabajosPK.equals(other.trabajosPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.Trabajos[ trabajosPK=" + trabajosPK + " ]";
    }
    
}
