/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Sistemas
 */
@Embeddable
public class ProyectoFasesPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private int id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "proyectos_id")
    private int proyectosId;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fases_id")
    private int fasesId;

    public ProyectoFasesPK() {
    }

    public ProyectoFasesPK(int id, int proyectosId, int fasesId) {
        this.id = id;
        this.proyectosId = proyectosId;
        this.fasesId = fasesId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProyectosId() {
        return proyectosId;
    }

    public void setProyectosId(int proyectosId) {
        this.proyectosId = proyectosId;
    }

    public int getFasesId() {
        return fasesId;
    }

    public void setFasesId(int fasesId) {
        this.fasesId = fasesId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (int) id;
        hash += (int) proyectosId;
        hash += (int) fasesId;
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProyectoFasesPK)) {
            return false;
        }
        ProyectoFasesPK other = (ProyectoFasesPK) object;
        if (this.id != other.id) {
            return false;
        }
        if (this.proyectosId != other.proyectosId) {
            return false;
        }
        if (this.fasesId != other.fasesId) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "modelo.ProyectoFasesPK[ id=" + id + ", proyectosId=" + proyectosId + ", fasesId=" + fasesId + " ]";
    }
    
}
